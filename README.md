# hotel-booking-service

This project is a development of simple microservice providing endpoints for hotel booking with CRUD operations for a booking

Services: BookingApi.

This service is written in Java, SpringBoot, In memory database h2 and Swagger for API documentation
Tests are written using Junit5, Mockito and Spring integration tests.


# Building and running the microservice

Use the following commands to build the RESTful service:

Using Gradle:

$cd hotel-booking-service

./gradlew build

Using Maven:

$cd hotel-booking-service

mvn clean install


Use the following commands to run the RESTful service in embedded container

Using Gradle:

$ cd hotel-booking-service

./gradlew run

Using Maven:

$cd hotel-booking-service

mvn spring-boot:run

# Access API documentation online

Please goto browser and open : http://localhost:8080/swagger-ui
You can explore the various available booking API http operations and test the methods.


# Testing the microservice

Use postman  or curl commands to test the API endpoints








package com.deloitte.hbs.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import java.util.Objects;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@ApiModel(description="All details about the booking. ")
public class Booking {

    @Id
    @GeneratedValue
    private long id;

    @Column(name = "nights")
    @ApiModelProperty(notes="Number of nights")
    private int nights;

    @Enumerated(value = EnumType.STRING)
    @Column(name = "booking_status")
    @ApiModelProperty(notes="Booking status")
    private BookingStatus bookingStatus;

    @ManyToOne
    @JoinColumn(name = "customer_id")
    @ApiModelProperty(notes="Customer of the booking")
    private Customer customer;

    @ManyToOne
    @JoinColumn(name = "hotel_id")
    @ApiModelProperty(notes="The hotel in the booking")
    private Hotel hotel;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getNights() {
        return nights;
    }

    public void setNights(int nights) {
        this.nights = nights;
    }

    public BookingStatus getBookingStatus() {
        return bookingStatus;
    }

    public void setBookingStatus(BookingStatus bookingStatus) {
        this.bookingStatus = bookingStatus;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Hotel getHotel() {
        return hotel;
    }

    public void setHotel(Hotel hotel) {
        this.hotel = hotel;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Booking booking = (Booking) o;
        return id == booking.id &&
                nights == booking.nights &&
                bookingStatus == booking.bookingStatus &&
                Objects.equals(customer, booking.customer) &&
                Objects.equals(hotel, booking.hotel);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, nights, bookingStatus, customer, hotel);
    }

    @Override
    public String toString() {
        return "Booking{" +
                "id=" + id +
                ", nights=" + nights +
                ", bookingStatus=" + bookingStatus +
                ", customer=" + customer +
                ", hotel=" + hotel +
                '}';
    }
}

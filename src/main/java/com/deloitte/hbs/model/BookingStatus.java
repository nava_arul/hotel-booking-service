package com.deloitte.hbs.model;

public enum BookingStatus {

    COMPLETED("Completed"), CANCELLED("Cancelled"), PENDING("Pending");

    private final String name;

    BookingStatus(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}

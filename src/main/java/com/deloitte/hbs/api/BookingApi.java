package com.deloitte.hbs.api;


import com.deloitte.hbs.data.BookingRepository;
import com.deloitte.hbs.model.Booking;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("/")
@Api(value = "This is hotel booking API")
public class BookingApi {

    private static final Logger LOG = LoggerFactory.getLogger(BookingApi.class);

    @Autowired
    BookingRepository bookingRepository;


    @GetMapping
    public String welcome() {
        return "Welcome to Hotel booking microservice";
    }


    @GetMapping("bookings")
    public List<Booking> getAllBookings() {
        LOG.info("Get all bookings");
        return bookingRepository.findAll();
    }


    @GetMapping("bookings/{id}")
    @ApiOperation(value = "Get a booking by id", notes = "This will return the booking for the given booking ID")
    public final Booking findBooking(@PathVariable(name = "id", required = true) Long id) throws BookingApiException {
        Objects.requireNonNull(id, "Cannot find. Booking id is mandatory");

        LOG.info("Reads a booking {}", id);
        return bookingRepository.findById(id).orElseThrow(() -> new BookingApiException("Booking", "id", id));
    }


    @DeleteMapping("bookings/{id}")
    @ApiOperation(value = "Deletes a booking for the given booking ID", response = ResponseEntity.class)
    public final ResponseEntity deleteBooking(@PathVariable(name = "id", required = true) Long id) throws BookingApiException {
        Objects.requireNonNull(id, "Cannot delete. Booking id is mandatory");
        LOG.info("Deleting a booking {}", id);
        final Booking booking = findBooking(id);
        bookingRepository.delete(booking);
        LOG.info("Delete complete");
        return new ResponseEntity("Booking deleted successfully", HttpStatus.OK);
    }


    @PostMapping("bookings")
    @ApiOperation(value = "Creates a new booking", notes = "The request payload must a JSON and cannot be empty.", response = ResponseEntity.class)
    public ResponseEntity createBooking(@RequestBody Booking booking) {
        Objects.requireNonNull(booking, "Cannot create Booking. Invalid request");

        if (bookingRepository.existsById(booking.getId())) {
            return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).contentType(MediaType.APPLICATION_JSON).body("Booking already exist");
        }

        LOG.info("Creating new booking {}", booking);
        final Booking savedBooking = bookingRepository.save(booking);

        LOG.info("Create complete. Booking : {}", savedBooking);
        return ResponseEntity.status(HttpStatus.OK).contentType(MediaType.APPLICATION_JSON).body("Booking created successfully");

    }

    @PutMapping("bookings/{id}")
    @ApiOperation(value = "Updates an existing booking", notes = "The request payload must a JSON and cannot be empty.", response = ResponseEntity.class)
    public ResponseEntity updateBooking(@RequestBody Booking booking, @PathVariable long id) {
        LOG.info("Updating a booking : {}", id);

        final Booking persistent = findBooking(id);
        persistent.setNights(booking.getNights());
        persistent.setCustomer(booking.getCustomer());
        persistent.setHotel(booking.getHotel());

        final Booking updated = bookingRepository.save(persistent);
        LOG.info("Booking updated. {}", updated);
        return new ResponseEntity("Booking updated successfully", HttpStatus.OK);
    }
}

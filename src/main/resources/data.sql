
insert into customer(id, name, email) values (1, 'Customer-A', 'Customer.A@mail.com');
insert into customer(id, name, email) values (2, 'Customer-B', 'Customer.B@mail.com');
insert into customer(id, name, email) values (3, 'Customer-C', 'Customer.C@mail.com');


insert into hotel(id, name) values (1, 'Hotel-A');
insert into hotel(id, name) values (2, 'Hotel-B');
insert into hotel(id, name) values (3, 'Hotel-C');
insert into hotel(id, name) values (4, 'Hotel-D');

insert into booking(id, nights, booking_status, customer_id, hotel_id) values (1, 3, 'COMPLETED', 1, 1);
insert into booking(id, nights, booking_status, customer_id, hotel_id) values (2, 5, 'COMPLETED', 1, 2);
insert into booking(id, nights, booking_status, customer_id, hotel_id) values (3, 7, 'PENDING', 1, 3);
insert into booking(id, nights, booking_status, customer_id, hotel_id) values (4, 7, 'PENDING', 1, 4);

insert into booking(id, nights, booking_status, customer_id, hotel_id) values (5, 3, 'COMPLETED', 2, 1);
insert into booking(id, nights, booking_status, customer_id, hotel_id) values (6, 5, 'COMPLETED', 2, 2);
insert into booking(id, nights, booking_status, customer_id, hotel_id) values (7, 7, 'PENDING', 2, 3);
insert into booking(id, nights, booking_status, customer_id, hotel_id) values (8, 7, 'PENDING', 2, 4);

insert into booking(id, nights, booking_status, customer_id, hotel_id) values (9, 3, 'COMPLETED', 3, 1);
insert into booking(id, nights, booking_status, customer_id, hotel_id) values (10, 5, 'COMPLETED', 3, 2);
insert into booking(id, nights, booking_status, customer_id, hotel_id) values (11, 7, 'PENDING', 3, 3);
insert into booking(id, nights, booking_status, customer_id, hotel_id) values (12, 7, 'PENDING', 3, 4);

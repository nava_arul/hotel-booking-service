package com.deloitte.hbs.api;

import com.deloitte.hbs.model.Booking;
import com.deloitte.hbs.util.TestDataUtil;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class BookingApiSpringBootTest {

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void testLoadContext() {
        Assert.assertTrue(true);
    }

    @Test
    public void testGETBookingsById() throws IOException, URISyntaxException {
        final ResponseEntity<String> responseEntity = this.restTemplate.exchange("/bookings/1", HttpMethod.GET, RequestEntity.EMPTY, String.class);
        String expected = new String(Files.readAllBytes(Paths.get(ClassLoader.getSystemResource("control/get-booking-by-id.json").toURI())));
        assertNotNull(responseEntity);
        assertEquals(200, responseEntity.getStatusCodeValue());
        assertEquals(expected, responseEntity.getBody());
    }

    @Test
    public void testGETReturns404WhenBookingIdNotFound() {
        final ResponseEntity<String> responseEntity = this.restTemplate.exchange("/bookings/02002", HttpMethod.GET, RequestEntity.EMPTY, String.class);
        assertNotNull(responseEntity);
        assertEquals(404, responseEntity.getStatusCodeValue());
        assertTrue(responseEntity.getBody().contains("Booking not found with id : '2002'"));
    }

    @Test
    public void testDELETEBookingsById() {
        final ResponseEntity<String> responseEntity = this.restTemplate.exchange("/bookings/1", HttpMethod.DELETE, RequestEntity.EMPTY, String.class);
        assertNotNull(responseEntity);
        assertEquals(200, responseEntity.getStatusCodeValue());
        assertEquals("Booking deleted successfully", responseEntity.getBody());
    }

    @Test
    public void testDELETEReturns404WhenBookingIdNotFound() {
        final ResponseEntity<String> responseEntity = this.restTemplate.exchange("/bookings/02002", HttpMethod.DELETE, RequestEntity.EMPTY, String.class);
        assertNotNull(responseEntity);
        assertEquals(404, responseEntity.getStatusCodeValue());
        assertTrue(responseEntity.getBody().contains("Booking not found with id : '2002'"));
    }

    @Test
    public void testPOSTCreateBookingFailsWhenBookingAlreadyExist() {
        final Booking booking = TestDataUtil.createBooking();
        HttpEntity<Booking> request = new HttpEntity<>(booking);
        final ResponseEntity<String> responseEntity = this.restTemplate.exchange("/bookings", HttpMethod.POST, request, String.class);
        assertNotNull(responseEntity);
        assertEquals(422, responseEntity.getStatusCodeValue());
        assertEquals("Booking already exist", responseEntity.getBody());
    }

    @Test
    public void testPOSTCreateBooking() {
        this.restTemplate.exchange("/bookings/1", HttpMethod.DELETE, RequestEntity.EMPTY, String.class);

        final Booking booking = TestDataUtil.createBooking();
        booking.setId(1);
        HttpEntity<Booking> request = new HttpEntity<>(booking);
        final ResponseEntity<String> responseEntity = this.restTemplate.exchange("/bookings", HttpMethod.POST, request, String.class);
        assertNotNull(responseEntity);
        assertEquals(200, responseEntity.getStatusCodeValue());
        assertEquals("Booking created successfully", responseEntity.getBody());
    }

    @Test
    public void testPUTUpdatesABooking() {

        final Booking booking = TestDataUtil.createBooking();
        HttpEntity<Booking> request = new HttpEntity<>(booking);
        final ResponseEntity<String> responseEntity = this.restTemplate.exchange("/bookings/1", HttpMethod.PUT, request, String.class);
        assertNotNull(responseEntity);
        assertEquals(200, responseEntity.getStatusCodeValue());
        assertEquals("Booking updated successfully", responseEntity.getBody());
    }

    @Test
    public void testPUTFailsWhenBookingNotExist() {

        final Booking booking = TestDataUtil.createBooking();
        HttpEntity<Booking> request = new HttpEntity<>(booking);
        final ResponseEntity<String> responseEntity = this.restTemplate.exchange("/bookings/2002", HttpMethod.PUT, request, String.class);
        assertNotNull(responseEntity);
        assertEquals(404, responseEntity.getStatusCodeValue());
        assertTrue(responseEntity.getBody().contains("Booking not found with id : '2002'"));
    }
}

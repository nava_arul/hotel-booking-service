package com.deloitte.hbs.api;


import com.deloitte.hbs.data.BookingRepository;
import com.deloitte.hbs.model.Booking;
import com.deloitte.hbs.util.TestDataUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class BookingApiTest {

    @InjectMocks
    BookingApi bookingApi;

    @Mock
    BookingRepository bookingRepository;

    @Test
    public void testFindBooking() {

        final Booking expected = TestDataUtil.createBooking();
        when(bookingRepository.findById(1L)).thenReturn(Optional.of(expected));
        final Booking booking = bookingApi.findBooking(1L);
        assertEquals(expected, booking);
    }

    @Test(expected = BookingApiException.class)
    public void testFindBookingThrowsExceptionWhenNoBookingFound() {

        final Booking expected = TestDataUtil.createBooking();
        when(bookingRepository.findById(1L)).thenReturn(Optional.empty());
        final Booking booking = bookingApi.findBooking(1L);
        assertEquals(expected, booking);

        verify(bookingRepository, atLeastOnce()).findById(1L);
    }


    @Test
    public void testDeleteBooking() {
        final Booking booking = TestDataUtil.createBooking();
        when(bookingRepository.findById(1L)).thenReturn(Optional.of(booking));
        doNothing().when(bookingRepository).delete(booking);
        final ResponseEntity<Object> response = bookingApi.deleteBooking(booking.getId());
        assertNotNull(response);
        assertTrue(response.getStatusCode() == HttpStatus.OK);
        assertEquals(200, response.getStatusCodeValue());
        assertEquals("Booking deleted successfully", response.getBody().toString());

        verify(bookingRepository, atLeastOnce()).findById(1L);
        verify(bookingRepository, atLeastOnce()).delete(booking);
    }

    @Test(expected = BookingApiException.class)
    public void testDeleteThrowsExceptionWhenNoBookingFound() {

        when(bookingRepository.findById(1L)).thenReturn(Optional.empty());
        bookingApi.deleteBooking(1L);

        verify(bookingRepository, atLeastOnce()).findById(1L);
    }

    @Test
    public void testUpdateBooking() {
        final Booking booking = TestDataUtil.createBooking();
        when(bookingRepository.findById(1L)).thenReturn(Optional.of(booking));
        when(bookingRepository.save(booking)).thenReturn(booking);
        final ResponseEntity<Booking> response = bookingApi.updateBooking(booking, booking.getId());
        assertNotNull(response);
        assertTrue(response.getStatusCode() == HttpStatus.OK);
        assertEquals(200, response.getStatusCodeValue());
        assertEquals("Booking updated successfully", response.getBody());

        verify(bookingRepository, atLeastOnce()).findById(1L);
        verify(bookingRepository, atLeastOnce()).save(booking);
    }

    @Test(expected = BookingApiException.class)
    public void testUpdateThrowsExceptionWhenNoBookingFound() {

        when(bookingRepository.findById(1L)).thenReturn(Optional.empty());
        bookingApi.updateBooking(TestDataUtil.createBooking(),1L);

        verify(bookingRepository, atLeastOnce()).findById(1L);
    }

    @Test
    public void testCreateBooking() {
        final Booking booking = TestDataUtil.createBooking();
        when(bookingRepository.save(booking)).thenReturn(booking);
        final ResponseEntity<Booking> response = bookingApi.createBooking(booking);
        assertNotNull(response);
        assertTrue(response.getStatusCode() == HttpStatus.OK);
        assertEquals(200, response.getStatusCodeValue());
        assertEquals("Booking created successfully", response.getBody());

        verify(bookingRepository, atLeastOnce()).save(booking);
    }
}

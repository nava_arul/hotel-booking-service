package com.deloitte.hbs.model;

import com.deloitte.hbs.util.TestDataUtil;
import org.junit.Test;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class HotelTest {

    @Test
    public void testHotelGetters(){
        Hotel hotel = TestDataUtil.createHotel();

        assertEquals(1, hotel.getId());
        assertEquals("Holiday Inn", hotel.getName());

        hotel.setName("Travelodge");
        assertEquals("Travelodge", hotel.getName());

    }

    @Test
    public void testEquals(){
        Hotel hotel = TestDataUtil.createHotel();
        assertTrue(hotel.equals(TestDataUtil.createHotel()));
    }
}

package com.deloitte.hbs.model;

import com.deloitte.hbs.util.TestDataUtil;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class BookingTest {

    @Test
    public void testBooking() {
        Booking booking = TestDataUtil.createBooking();


        assertEquals(1L, booking.getId());
        assertEquals(3, booking.getNights());
        assertEquals(BookingStatus.COMPLETED, booking.getBookingStatus());
        assertEquals("customer.email@gmail.com", booking.getCustomer().getEmail());
        assertEquals("SimpleCustomer", booking.getCustomer().getName());
        assertEquals(2L, booking.getCustomer().getId());
        assertEquals(1, booking.getHotel().getId());
        assertEquals("Holiday Inn", booking.getHotel().getName());
    }

    @Test
    public void testEquals(){

        Booking booking = TestDataUtil.createBooking();
        assertEquals(booking, TestDataUtil.createBooking());

        Map<Booking, Long> bookingLongMap = new HashMap<>();
        bookingLongMap.put(booking, booking.getId());
        assertTrue(1L == bookingLongMap.get(booking));


        final int hashCode = booking.hashCode();
        booking.setNights(123);
        assertFalse(hashCode == booking.hashCode());

    }

}

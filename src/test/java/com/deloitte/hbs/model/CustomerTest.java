package com.deloitte.hbs.model;

import com.deloitte.hbs.util.TestDataUtil;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class CustomerTest {

    @Test
    public void testCustomer(){
        Customer customer = TestDataUtil.createCustomer();

        assertEquals("customer.email@gmail.com", customer.getEmail());
        assertEquals("SimpleCustomer", customer.getName());
        assertEquals(2L, customer.getId());

        customer.setName("TwinCustomer");
        customer.setEmail("TwinCustomer@mail.com");
        assertEquals("TwinCustomer@mail.com", customer.getEmail());
        assertEquals("TwinCustomer", customer.getName());
    }

    @Test
    public void testEquals(){
        Customer customer = TestDataUtil.createCustomer();
        assertTrue(customer.equals(TestDataUtil.createCustomer()));

        Map<Customer, Long> customerMap = new HashMap<>();
        customerMap.put(customer, customer.getId());

        assertTrue(2L==customerMap.get(customer));

    }
}

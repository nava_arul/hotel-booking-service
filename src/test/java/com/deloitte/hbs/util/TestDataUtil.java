package com.deloitte.hbs.util;

import com.deloitte.hbs.model.Booking;
import com.deloitte.hbs.model.BookingStatus;
import com.deloitte.hbs.model.Customer;
import com.deloitte.hbs.model.Hotel;

public class TestDataUtil {

    public static Customer createCustomer() {
        Customer customer = new Customer();
        customer.setEmail("customer.email@gmail.com");
        customer.setName("SimpleCustomer");
        customer.setId(2L);
        return customer;
    }

    public static Hotel createHotel() {
        Hotel hotel = new Hotel();
        hotel.setId(1);
        hotel.setName("Holiday Inn");
        return hotel;
    }

    public static Booking createBooking(){
        Booking booking = new Booking();
        booking.setHotel(createHotel());
        booking.setCustomer(createCustomer());
        booking.setNights(3);
        booking.setBookingStatus(BookingStatus.COMPLETED);
        booking.setId(1L);
        return booking;
    }

    public static Booking createNewBooking(){
        Booking booking = new Booking();
        booking.setHotel(createHotel());
        booking.setCustomer(createCustomer());
        booking.setNights(3);
        booking.setBookingStatus(BookingStatus.COMPLETED);
        return booking;
    }
}
